# instagramapp
To run application:

- in root folder where there is 'Podfile' file, run 'pod install' to install all Pods dependencies

- open instagram.xcworkspace in Xcode

- replace right configs in InstagramConfig.swift file

- run with any iOS simulator >= 9.3

Improvements:

- Add UI Automation test

- Add loading progress to WKWebView when loading login screen. In Edge network, the login loading is not so fast.

- Load instagram post more intelligent by using max_id & min_id parameter.

- Photo viewer: make it zoomable by double tap or pan gesture.
