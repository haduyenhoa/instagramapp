//
//  InstagramAccessToken.swift
//  instagram
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

struct AccessToken {
    var token: String
    
    enum CodingKeys: String, CodingKey {
        case token = "access_token"
    }
}

extension AccessToken: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(token, forKey: .token)
    }
}

extension AccessToken: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        token = try values.decode(String.self, forKey: .token)
    }
}


