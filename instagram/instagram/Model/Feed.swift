//
//  Photo.swift
//  instagram
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

// MARK: Feed
struct Feed {
    var posts: [InstagramPost]
    
    enum CodingKeys: String, CodingKey {
        case posts = "data"
    }
}

extension Feed: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(posts, forKey: .posts)
    }
}

extension Feed: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        posts = try values.decode([InstagramPost].self, forKey: .posts)
    }
}

// MARK: Instagram Post
struct InstagramPost {
    var id: String
    var createdTime: Date
    var tags: [String]
    var likes: [String: Int]
    var comments: [String: Int]
    var images: [ImageType: InstagramImage]
    
    enum CodingKeys: String, CodingKey {
        case id
        case tags
        case images
        case likes
        case comments
        case createdTime = "created_time"
    }
}

extension InstagramPost: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(id, forKey: .id)
        try container.encode(tags, forKey: .tags)
        let dict = images.reduce(into: [String: InstagramImage]()) { (result: inout [String: InstagramImage], arg) in
            result.updateValue(arg.value, forKey: arg.key.rawValue)
        }
        
        try container.encode(dict, forKey: .images)
        try container.encode(likes, forKey: .likes)
        try container.encode(comments, forKey: .comments)
        
        let timeInterval =  "\(createdTime.timeIntervalSince1970)"
        try container.encode(timeInterval, forKey: .createdTime)
    }
}

extension InstagramPost: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        tags = try values.decode([String].self, forKey: .tags)
        let dict = try values.decode([String: InstagramImage].self, forKey: .images)
        images = dict.reduce(into: [ImageType: InstagramImage]()) { ( result: inout [ImageType: InstagramImage], arg) in
            if let imageType = ImageType(rawValue: arg.key) {
                result.updateValue(arg.value, forKey: imageType)
            }
        }
        
        let timeInterval = try values.decode(String.self, forKey: .createdTime)
        createdTime = Date(timeIntervalSince1970: Double(timeInterval) ?? 0)
        
        likes = try values.decode([String: Int].self, forKey: .likes)
        comments = try values.decode([String: Int].self, forKey: .comments)
    }
}

// MARK: Instagram Image
enum ImageType: String {
    case thumbnail = "thumbnail"
    case lowResolution = "low_resolution"
    case standard = "standard_resolution"
}

struct InstagramImage {
    var width: Int
    var height: Int
    var url: String
    
    enum CodingKeys: String, CodingKey {
        case width
        case height
        case url
    }
}

extension InstagramImage: Encodable {
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(width, forKey: .width)
        try container.encode(height, forKey: .height)
        try container.encode(url, forKey: .url)
    }
}

extension InstagramImage: Decodable {
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        width = try values.decode(Int.self, forKey: .width)
        height = try values.decode(Int.self, forKey: .height)
        url = try values.decode(String.self, forKey: .url)
    }
}
