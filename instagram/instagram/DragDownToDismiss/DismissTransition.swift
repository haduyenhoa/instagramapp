//
//  DismissAnimator.swift
//  instagram
//
//  Created by HDH on 02/04/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit

class DismissTransition: NSObject {

}

extension DismissTransition : UIViewControllerAnimatedTransitioning {
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from),
            let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to) else {
                return //viewControllers not defined
        }
        
        transitionContext.containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)

        let newOrigin = CGPoint(x: 0, y: UIScreen.main.bounds.height)
        let frame = CGRect(origin: newOrigin, size: UIScreen.main.bounds.size)
        
        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromViewController.view.frame = frame
        }) { (_) in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        }
    }
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }
}
