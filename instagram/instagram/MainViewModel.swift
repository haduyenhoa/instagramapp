//
//  MainViewModel.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import GRDB
import RxGRDB
import Moya
import RxSwift

/**
 View Model who controls MainViewController (displays my recent medias as grid layout)
 */
class MainViewModel: NSObject {
    var isTokenInvalid: BehaviorSubject<Bool>
    var posts = BehaviorSubject<[InstagramPostRecord]>(value: [])
    
    fileprivate let dbQueue: DatabaseQueue
    private let request = SQLRequest("SELECT * from posts order by createdTime desc")
    private var minId: String? = nil //use this to make request to retrieve medias more recent than the newest media retrieved in DB
    private var lastUpdate: Date?
    
    init(dbQueue: DatabaseQueue) {
        self.dbQueue = dbQueue
        isTokenInvalid = BehaviorSubject<Bool>(value: (KeychainStore.getAccessToken()?.isEmpty ?? true))
        
        super.init()
        
        _ = request.rx.changes(in: self.dbQueue).subscribe(onNext: { [weak self] (db) in
            let allPosts = (try? InstagramPostRecord.fetchAll(db)) ?? [InstagramPostRecord]() //should be moved to DatabaseManager
            self?.minId = allPosts.first?.postId
            self?.posts.onNext(allPosts)
        }, onError: nil, onCompleted: nil, onDisposed: nil)
    }
    
    func refreshPost() -> Observable<[InstagramPostRecord]> {
        return Observable.create { observer in
            if let accessToken = KeychainStore.getAccessToken() {
                let instagramProvider = MoyaProvider<InstagramAPI>()
                let task = instagramProvider.request(.getMyRecentMedia(accessToken: accessToken, minId: self.minId), completion: {[weak self] (result) in
                    if case let .success(response) = result {
                        let jsonDecoder = JSONDecoder()
                        if let feed = try? jsonDecoder.decode(Feed.self, from: response.data), let _dbQueue = self?.dbQueue {
                            //update DB
                            let records = feed.posts.map { InstagramPostRecord(post: $0) }
                            
                            DatabaseManager.write(dbQueue: _dbQueue, records: records)
                            self?.lastUpdate = Date()
                            observer.onNext(records)
                        } else {
                            print("Got error while getting Feed, maybe access_token is expired, request new one?");
                            self?.isTokenInvalid.onNext(true)
                            observer.onError(RxError.unknown)
                        }
                    } else if let error = result.error {
                        observer.onError(error)
                    }
                })
                
                return Disposables.create {
                    task.cancel()
                }
            } else {
                self.isTokenInvalid.onNext(true)
                observer.onNext([])
                return Disposables.create {
                    
                }
            }
        }
    }
}
