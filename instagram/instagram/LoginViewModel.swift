//
//  LoginViewModel.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import WebKit
import RxSwift
import Moya

/**
 View Model who controls LoginViewController
 */
class LoginViewModel: NSObject {
    let code: BehaviorSubject<String?>
    let accessToken: BehaviorSubject<String?>

    var errorGettingCode: String?
    
    override init() {
        code = BehaviorSubject(value: nil)
        accessToken = BehaviorSubject(value: nil)
        
        super.init()
        
        _ = code.asObservable().subscribe(onNext: {  [weak self] (code) in
            if let _code = code {
                self?.getAccessToken(code: _code)
            }
        }, onError: nil, onCompleted: nil)
    }
    
    fileprivate func getAccessToken(code: String) {
        let instagramProvider = MoyaProvider<InstagramAPI>()
        instagramProvider.request(.getAccessToken(code: code), callbackQueue: nil, progress: nil) {[weak self] (result) in
            if case let .success(response) = result {
                let jsonDecoder = JSONDecoder()
                if let accessToken = try? jsonDecoder.decode(AccessToken.self, from: response.data) {
                    do {
                        _ = try KeychainStore.updateAccessToken(token: accessToken.token)
                    } catch {
                        assertionFailure("Cannot update keychain, this should crash while debuggin. Errror = \(error)")
                    }
                    self?.accessToken.onNext(accessToken.token)
                } else {
                    self?.errorGettingCode = R.string.localizable.error_access_instagram_api()
                }
            }
        }
    }
}

extension LoginViewModel: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didReceiveServerRedirectForProvisionalNavigation navigation: WKNavigation!) {
        if let url = webView.url,
            url.absoluteString.starts(with: "\(InstagramConfig.redirect_uri)/?code="),
            let urlComponents = URLComponents.init(url: url, resolvingAgainstBaseURL: false),
            let firstQueryItem = urlComponents.queryItems?.first,
            firstQueryItem.name == "code" {
            
            if let code = firstQueryItem.value {
                self.code.onNext(code)
                self.errorGettingCode = nil
            } else {
                self.code.onNext(nil)
                self.errorGettingCode = R.string.localizable.error_access_instagram_api()
            }
        }
    }
}
