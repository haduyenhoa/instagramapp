//
//  PhotoViewerViewController.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoViewerViewController: UIPageViewController {

    private var panGesture: UIPanGestureRecognizer?
    var interactor: PanInteractor?
    private var viewModel: PhotoViewerViewModel!
    
    init(viewModel: PhotoViewerViewModel) {
        self.viewModel = viewModel
        
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
        
        self.delegate = self
        self.dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //pan gesture to dismiss
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(PhotoViewerViewController.handlePan(_:)))
        self.view.addGestureRecognizer(panGesture!)
        
        if let startViewController = pages(index: self.viewModel.startIndex) {
            setViewControllers([startViewController], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        } else {
            //this should not happen
            self.setViewControllers([UIViewController(nibName: nil, bundle: nil)], direction: UIPageViewControllerNavigationDirection.forward, animated: true, completion: nil)
        }
    }
    
    //drag down to dismiss
    @objc func handlePan(_ sender:UIPanGestureRecognizer) {
        guard let _interactor = interactor else {
            return //interactor not set
        }
        
        let percentThreshold:CGFloat = 0.3
        
        let translation = sender.translation(in: view)
        let verticalMovement = translation.y / view.bounds.height
        let downwardMovement = fmaxf(Float(verticalMovement), 0.0)
        let downwardMovementPercent = fminf(downwardMovement, 1.0)
        let progress = CGFloat(downwardMovementPercent)
        
        switch sender.state {
        case .began:
            _interactor.started = true
            dismiss(animated: true, completion: nil)
        case .changed:
            _interactor.shouldFinish = progress > percentThreshold
            _interactor.update(progress)
        case .cancelled:
            _interactor.started = false
            _interactor.cancel()
        case .ended:
            _interactor.started = false
            _interactor.shouldFinish
                ? _interactor.finish()
                : _interactor.cancel()
        default:
            break
        }
    }
}

extension PhotoViewerViewController: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let currentViewController = viewController as? PhotoItemViewController else {
            return nil //this pagecontroller does not allow other viewcontroller than PhotoItemViewController
        }
        
        let nextIndex = currentViewController.pageIndex - 1
        return pages(index: nextIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let currentViewController = viewController as? PhotoItemViewController else {
            return nil //this pagecontroller does not allow other viewcontroller than PhotoItemViewController
        }
        
        let nextIndex = currentViewController.pageIndex + 1
        return pages(index: nextIndex)
    }
    
    fileprivate func pages(index: Int) -> UIViewController? {
        if index < 0 || index >= self.viewModel.posts.count {
            return nil
        }
        
        let viewController = R.storyboard.main.photoItemViewController()
        
        //TODO - improvement: move this to a ViewModel
        viewController?.imageUrl = viewModel.posts[index].standardResolutionUrl
        viewController?.pageIndex = index
        viewController?.placeHolderImage = SDImageCache.shared().imageFromDiskCache(forKey: viewModel.posts[index].lowResolutionUrl)
        
        return viewController
    }
}

extension PhotoViewerViewController: UIPageViewControllerDelegate {
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if completed && previousViewControllers.count > 0 && self.viewModel.posts.count > 1 {
            
        }
    }
}
