//
//  MainViewController.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import Moya
import RxSwift
import SVProgressHUD
import Alamofire

class MainViewController: UIViewController {
    private var viewModel: MainViewModel!
    private  let interactor = PanInteractor()
    
    fileprivate var posts = [InstagramPostRecord]()
    fileprivate var collectionView: UICollectionView!
    
    fileprivate var isFirstLoad = true
    private let refreshControl = UIRefreshControl()
    
    required init(viewModel: MainViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        super.loadView()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        
        collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: layout)
        collectionView.dataSource = self
        collectionView.delegate = self
        
        collectionView.register(PhotoCell.self, forCellWithReuseIdentifier: "PhotoCell")
        collectionView.backgroundColor = UIColor.white
        view = collectionView
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //setup HUD
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.dark)
        SVProgressHUD.setDefaultMaskType(.black)
        
        //observes
        _ = viewModel.isTokenInvalid.subscribe(onNext: { (isInvalid) in
            if (isInvalid) {
                let loginViewModel = LoginViewModel()
                let loginViewController = LoginViewController(viewModel: loginViewModel)
                self.present(loginViewController, animated: true, completion: nil)
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil)
        
        _ = viewModel.posts.subscribe(onNext: {[weak self] (posts) in
            self?.posts.removeAll()
            self?.posts.append(contentsOf: posts)
            self?.posts.sort(by: { (postA, postB) -> Bool in
                postA.createdTime > postB.createdTime
            })
            
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
            
            }, onError: nil, onCompleted: nil, onDisposed: nil)

        //pull to refresh
        collectionView.alwaysBounceVertical = true
        refreshControl.tintColor = UIColor.red
        refreshControl.attributedTitle = NSAttributedString(string: R.string.localizable.message_getting_media())
        refreshControl.addTarget(self, action: #selector(MainViewController.refreshData), for: .valueChanged)

        collectionView.addSubview(refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let networkManager = NetworkReachabilityManager()
        
        if isFirstLoad && (networkManager?.isReachable ?? false) {
            isFirstLoad = false
            SVProgressHUD.show(withStatus: R.string.localizable.message_getting_media())
            _ = viewModel.refreshPost().subscribe(onNext: { (_) in
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss(withDelay: 1.0)
                }
            }, onError: nil, onCompleted: nil, onDisposed: nil)
        }
    }
    
    @objc private func refreshData() {
        let networkManager = NetworkReachabilityManager()
        if !(networkManager?.isReachable ?? false) {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
            
            return
        }
        
        _ = viewModel.refreshPost().subscribe(onNext: { [weak self] (_) in
            DispatchQueue.main.async {
                self?.refreshControl.endRefreshing()
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil)
    }
}

extension MainViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.posts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PhotoCell", for: indexPath) as! PhotoCell
        cell.update(post: posts[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 6.0
    }
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //show photo
        let viewModel = PhotoViewerViewModel(posts: self.posts, startIndex: indexPath.row)
        let photoViewController = PhotoViewerViewController(viewModel: viewModel)
        
        photoViewController.interactor = self.interactor
        photoViewController.transitioningDelegate = self
        
        self.present(photoViewController, animated: true, completion: nil)
    }
}

extension MainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = (collectionView.bounds.width - 6)/2
        
        return CGSize(width: cellWidth, height: cellWidth)
    }
}

extension MainViewController: UIViewControllerTransitioningDelegate {
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return DismissTransition()
    }
    
    func interactionControllerForDismissal(using animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        return interactor.started ? interactor : nil
    }
}
