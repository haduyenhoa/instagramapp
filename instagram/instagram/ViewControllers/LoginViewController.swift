//
//  LoginViewController.swift
//  instagram
//
//  Created by Duyen Hoa Ha on 29.03.18.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import WebKit
import RxSwift
import SVProgressHUD
import Moya
import Masonry


class LoginViewController: UIViewController {
    
    var webView: WKWebView
    var viewModel: LoginViewModel
    
    required init(viewModel: LoginViewModel) {
        self.viewModel = viewModel
        
        let webConfiguration = WKWebViewConfiguration()
        webConfiguration.websiteDataStore = WKWebsiteDataStore.nonPersistent()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self.viewModel
        
        super.init(nibName: nil, bundle: nil)
        
        //setup HUD
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.light)
        SVProgressHUD.setDefaultMaskType(.black)
        
        //observer code and access token
        _ = self.viewModel.code.asObservable().subscribe(onNext: { (code) in
            if !(code?.isEmpty ?? true) {
                DispatchQueue.main.async {
                    SVProgressHUD.show(withStatus: R.string.localizable.initialising_instagram())
                }
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil)
        
        _ = self.viewModel.accessToken.asObservable().subscribe(onNext: { (accessToken) in
            if !(accessToken?.isEmpty ?? true) {
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss(completion: {
                        self.dismiss(animated: true, completion: nil)
                    })
                }
            }
        }, onError: nil, onCompleted: nil, onDisposed: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        let backgroundView = UIView(frame: CGRect.zero)
        backgroundView.backgroundColor = UIColor.white
        view = backgroundView
        
        backgroundView.addSubview(webView)
        webView.mas_makeConstraints { (maker) in
            maker?.top.equalTo()(view.mas_topMargin)
            maker?.left.right().bottom().equalTo()(view)
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        
        let storage = HTTPCookieStorage.shared
        storage.cookieAcceptPolicy = .always
        
        if let request = InstagramServices.createGetCodeRequest() {
            webView.load(request)
        }
    }
}
