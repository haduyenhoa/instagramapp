//
//  PhotoItemViewController.swift
//  instagram
//
//  Created by HDH on 02/04/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire

class PhotoItemViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    var pageIndex: Int = -1
    var imageUrl: String?
    var placeHolderImage: UIImage?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        if let urlPath = imageUrl {
            if let image = SDImageCache.shared().imageFromDiskCache(forKey: urlPath) {
                DispatchQueue.main.async {
                    UIView.transition(with: self.imageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                        self.imageView.image = image
                    }, completion: nil)
                }
            } else {
                if let image = placeHolderImage {
                    DispatchQueue.main.async {
                        UIView.transition(with: self.imageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                            self.imageView.image = image
                        }, completion: nil)
                    }
                }
                
                if (NetworkReachabilityManager()?.isReachable ?? false) {
                    self.imageView.sd_setImage(with: URL(string: urlPath), completed: nil)
                } else {
                    print("No internet connection, will try to download later")
                }
            }
        }
    }

}
