//
//  ViewController.swift
//  instagram
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import Rswift

class ViewController: UIViewController {
    let loginViewModel = LoginViewModel()

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if InstagramConfig.client_scret.isEmpty || InstagramConfig.client_id.isEmpty {
            let alertViewController = UIAlertController(title: R.string.localizable.message_information(),
                                                        message: R.string.localizable.error_missing_configuration(),
                                                        preferredStyle: .alert)
            let okButton = UIAlertAction(title: R.string.localizable.btn_got_it(),
                                         style: .default,
                                         handler: nil)
            
            alertViewController.addAction(okButton)
            self.present(alertViewController, animated: true, completion: nil)
            return
        }
        
        if !(KeychainStore.getAccessToken()?.isEmpty ?? true) {
            let mainViewModel = MainViewModel(dbQueue: dbQueue)
            let mainViewController = MainViewController(viewModel: mainViewModel)
            self.present(mainViewController, animated: false, completion: nil)
        } else {
            let loginViewController = LoginViewController(viewModel: loginViewModel)
            self.present(loginViewController, animated: true, completion: nil)
        }
    }
}

