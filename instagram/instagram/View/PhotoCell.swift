//
//  PhotoCell.swift
//  instagram
//
//  Created by HDH on 02/04/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import UIKit
import Masonry
import SDWebImage
import Alamofire

class PhotoCell: UICollectionViewCell {
    private var mainImageView: UIImageView!
    private var likeImageView: UIImageView!
    private var commentImageView: UIImageView!
    private var footerView: UIView!
    
    var likeLabel: UILabel!
    var commentLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        mainImageView = UIImageView()
        mainImageView.contentMode = .scaleAspectFill
        mainImageView.backgroundColor = UIColor.black
        
        likeImageView = UIImageView()
        likeImageView.image = R.image.like_25()
        likeImageView.contentMode = .scaleAspectFit
        
        commentImageView = UIImageView()
        commentImageView.image = R.image.comment_25()
        commentImageView.contentMode = .scaleAspectFit
        
        likeLabel = UILabel()
        likeLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
        likeLabel.textColor = UIColor.black
        
        commentLabel = UILabel()
        commentLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
        commentLabel.textColor = UIColor.black
        
        footerView = UIView();
        footerView.backgroundColor = UIColor.white
        footerView.alpha = 0.5

        contentView.addSubview(mainImageView)
        contentView.addSubview(footerView)
        
        contentView.addSubview(likeImageView)
        contentView.addSubview(commentImageView)
        contentView.addSubview(likeLabel)
        contentView.addSubview(commentLabel)
        
        mainImageView.mas_makeConstraints {(maker) in
            maker?.edges.equalTo()(contentView)
        }
        
        footerView.mas_makeConstraints {(maker) in
            maker?.left.right().bottom().equalTo()(contentView)
            maker?.height.equalTo()(40)
        }
        
        likeLabel.mas_makeConstraints {(maker) in
            maker?.centerY.equalTo()(footerView)
            maker?.height.equalTo()(14)
            maker?.left.equalTo()(contentView)?.offset()(10)
        }
        
        likeImageView.mas_makeConstraints {(maker) in
            maker?.left.equalTo()(likeLabel.mas_right)?.offset()(5)
            maker?.height.with().equalTo()(30)
            maker?.centerY.equalTo()(likeLabel)
        }
        
        commentImageView.mas_makeConstraints {(maker) in
            maker?.right.equalTo()(contentView)?.offset()(-10)
            maker?.height.with().equalTo()(30)
            maker?.centerY.equalTo()(likeImageView)
        }
        
        commentLabel.mas_makeConstraints {(maker) in
            maker?.right.equalTo()(commentImageView.mas_left)?.offset()(-5)
            maker?.height.equalTo()(14)
            maker?.centerY.equalTo()(commentImageView)
        }
    }
    
    func update(post: InstagramPostRecord) {
        if let lowResolutionUrl = post.lowResolutionUrl {
            if let image = SDImageCache.shared().imageFromDiskCache(forKey: lowResolutionUrl) {
                DispatchQueue.main.async {
                    UIView.transition(with: self.mainImageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                        self.mainImageView.image = image
                    }, completion: nil)
                }
            } else if (NetworkReachabilityManager()?.isReachable ?? false) {
                let imageManager = SDWebImageManager.shared()
                imageManager.imageDownloader?.downloadImage(with: URL(string: lowResolutionUrl), options: .continueInBackground, progress: nil, completed: { (image, data, error, cached) in
                    SDImageCache.shared().store(image, forKey: lowResolutionUrl, completion: nil)
                    DispatchQueue.main.async {
                        UIView.transition(with: self.mainImageView, duration: 0.5, options: UIViewAnimationOptions.transitionCrossDissolve, animations: {
                            self.mainImageView.image = image
                        }, completion: nil)
                    }
                })
            } else {
                print("No internet connection, will try to download later")
            }
        }

        likeLabel.text = "\(post.likeCount)"
        commentLabel.text = "\(post.commentCount)"
        
        likeLabel.alpha = post.likeCount > 0 ? 1.0 : 0.0
        likeImageView.alpha = likeLabel.alpha
        
        commentLabel.alpha = post.commentCount > 0 ? 1.0 : 0.0
        commentImageView.alpha = commentLabel.alpha

        footerView.isHidden = post.likeCount == 0 && post.commentCount == 0
        
        self.updateConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
