//
//  KeychainStore.swift
//  instagram
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import Security
import Rswift

/**
 Helper class to support add/update/remove/query some important information (in String) into device's Keychain.
 */
class KeychainStore {
    fileprivate let errorDomain = "com.haduyenhoa.instagram.keychain"
    fileprivate let keychainAccount = "com.haduyenhoa.instagram.access_token"
    
    static func getAccessToken() -> String? {
        let getTokenQuery: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                            kSecAttrAccount as String: KeychainStore().keychainAccount,
                                            kSecMatchLimit as String: kSecMatchLimitOne,
                                            kSecReturnData as String: kCFBooleanTrue]
        
        var keychainItem: CFTypeRef?
        let status = SecItemCopyMatching(getTokenQuery as CFDictionary, &keychainItem)
        guard status == errSecSuccess else {
            return nil //no access token found
        }
        
        if let retrievedData = keychainItem as? Data, let accessToken = String(data: retrievedData, encoding: .utf8) {
            return accessToken
        } else {
            return nil //this should not happen
        }
    }
    
    static func updateAccessToken(token: String) throws -> Bool {
        if getAccessToken()?.compare(token) == .orderedSame {
            return true //no need to update token
        }
        
        var status: OSStatus?
        if !(getAccessToken()?.isEmpty ?? true) {
            //update
            let updateTokenQuery: [String: Any] = [kSecClass as String: kSecClassGenericPassword]
            let updateTokenAttribute: [String: Any] = [kSecAttrAccount as String: KeychainStore().keychainAccount,
                                                       kSecValueData as String: token.data(using: .utf8)!]
            
            status = SecItemUpdate(updateTokenQuery as CFDictionary, updateTokenAttribute as CFDictionary)
        } else {
            //insert
            let insertTokenQuery: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                                   kSecAttrAccount as String: KeychainStore().keychainAccount,
                                                   kSecValueData as String: token.data(using: .utf8)!]
            status = SecItemAdd(insertTokenQuery as CFDictionary, nil)
        }
        
        
        guard status == errSecSuccess else {
            throw NSError.init(domain: KeychainStore().errorDomain,
                               code: 101,
                               userInfo: ["message": R.string.localizable.error_write_token(), "writeStatus": status ?? ""])
        }
        
        return true
    }
    
    static func removeAccessToken() throws {
        let removeKeychainQuery: [String: Any] = [kSecClass as String: kSecClassGenericPassword,
                                               kSecAttrAccount as String: KeychainStore().keychainAccount]
        
        let status = SecItemDelete(removeKeychainQuery as CFDictionary)
        guard status == errSecSuccess else {
            throw NSError.init(domain: KeychainStore().errorDomain,
                               code: 102,
                               userInfo: ["message": R.string.localizable.error_read_token(), "writeStatus": status])
        }
    }
}
