//
//  InstagramAPI.swift
//  instagram
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

/**
 Single environment with fixed config.
 We may think about some dynamic config who support multiple environment: Dev, Staging, Prod
 */
struct InstagramConfig {
    //MARK: commun
    static let client_id = ""
    static let client_scret = ""
    static let redirect_uri = "http://www.igenius.net"
    static let grant_type = "authorization_code"
    
    //MARK: to get code
    static let scope = "public_content"
    static let get_code_response_type = "code"
}

/**
 Use it to retrieve the endpoint of get code service
 */
class InstagramServices {
    static func createGetCodeRequest() -> URLRequest? {
        guard let url = URL(string: InstagramServices().getCodeUrl()) else {
            return nil
        }
        
        return URLRequest(url: url)
    }
    
    // MARK: Private functions
    private func getCodeUrl() -> String {
        return "https://api.instagram.com/oauth/authorize/"
            + "?client_id=\(InstagramConfig.client_id)"
            + "&redirect_uri=\(InstagramConfig.redirect_uri)"
            + "&response_type=code"
            + "&scope=\(InstagramConfig.scope)"
    }
}


