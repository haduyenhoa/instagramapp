//
//  DatabaseManager.swift
//  instagram
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import GRDB

class DatabaseManager {
    /**
     Database initialisation: create table posts if not exist
    */
    static func initDatabase(dbQueue: DatabaseQueue) {
        do {
            try dbQueue.write { db in
                try db.create(table: InstagramPostRecord.databaseTableName, temporary: false, ifNotExists: true, body: { t in
                    t.column(InstagramPostRecord.ColumnName.postId.rawValue, .text).primaryKey()
                    t.column(InstagramPostRecord.ColumnName.tags.rawValue, .text).notNull()
                    t.column(InstagramPostRecord.ColumnName.thumbnailUrl.rawValue, .text)
                    t.column(InstagramPostRecord.ColumnName.lowResolutionUrl.rawValue, .text)
                    t.column(InstagramPostRecord.ColumnName.standardResolutionUrl.rawValue, .text)
                    t.column(InstagramPostRecord.ColumnName.createdTime.rawValue, .date)
                    t.column(InstagramPostRecord.ColumnName.likeCount.rawValue, .integer)
                    t.column(InstagramPostRecord.ColumnName.commentCount.rawValue, .integer)
                })
            }
        } catch {
            print("Cannot setup Database")
        }
    }
    
    /**
     Insert or update a post
     */
    static func write(dbQueue: DatabaseQueue, records: [InstagramPostRecord]) {
        try? dbQueue.inTransaction { db in
            let idColume = Column(InstagramPostRecord.ColumnName.postId.rawValue)
            for r in records {
                if (try InstagramPostRecord.filter(idColume == r.postId).fetchOne(db) == nil) {
                    try r.insert(db)
                } else {
                    try r.update(db)
                }
            }
            return .commit
        }
    }
}
