//
//  InstagramAPI.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import Moya

/**
 By using Moya framework, this class allows to open a connection to an endpoint to retrieve expected datas: access token, my recent medias, etc...
 */
enum InstagramAPI {
    case getAccessToken(code: String)
    case getMyRecentMedia(accessToken: String, minId: String?)
}

extension InstagramAPI: TargetType {
    var baseURL: URL {
        switch self {
        case .getAccessToken(_):
            return URL(string: "https://api.instagram.com/oauth/access_token")!
        case .getMyRecentMedia(let accessToken, let minId):
            var urlPath = "https://api.instagram.com/v1/users/self/media/recent/?access_token=\(accessToken)"
            //by using this, we won't load old post (update on those posts will be ignored: like & comments)
            if let _minId = minId {
                urlPath += "&min_id=\(_minId)"
            }
            
            return URL(string: urlPath)!
        }
    }
    
    var path: String {
        switch self {
        case .getAccessToken(_):
            return ""
        case .getMyRecentMedia(_, _):
            return ""
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getAccessToken(_):
            return .post
        case .getMyRecentMedia(_, _):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getAccessToken(let code):
            let clientIdFormData = MultipartFormData(provider: .data(InstagramConfig.client_id.data(using: .utf8)!),
                                                     name: "client_id",
                                                     fileName: nil,
                                                     mimeType: nil)
            let clientSecretFormData = MultipartFormData(provider: .data(InstagramConfig.client_scret.data(using: .utf8)!),
                                                         name: "client_secret",
                                                         fileName: nil,
                                                         mimeType: nil)
            let grantTypeFormData = MultipartFormData(provider: .data(InstagramConfig.grant_type.data(using: .utf8)!),
                                                      name: "grant_type",
                                                      fileName: nil,
                                                      mimeType: nil)
            let redirectUriFormdata = MultipartFormData(provider: .data(InstagramConfig.redirect_uri.data(using: .utf8)!),
                                                        name: "redirect_uri",
                                                        fileName: nil,
                                                        mimeType: nil)
            let codeFormData = MultipartFormData(provider: .data(code.data(using: .utf8)!),
                                         name: "code",
                                         fileName: nil,
                                         mimeType: nil)

            return .uploadMultipart([clientIdFormData,
                                         clientSecretFormData,
                                         grantTypeFormData,
                                         redirectUriFormdata,
                                         codeFormData])
        case .getMyRecentMedia(_, _):
            return Task.requestPlain
        }
    }
    
    var headers: [String : String]? {
        return nil
    }
}
