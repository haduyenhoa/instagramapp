//
//  InstagramPostRecord.swift
//  instagram
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation
import GRDB

class InstagramPostRecord: Record {
    enum ColumnName: String {
        case postId = "postId"
        case likeCount = "likeCount"
        case commentCount = "commentCount"
        case tags = "tags"
        case thumbnailUrl = "thumbnail"
        case lowResolutionUrl = "lowResolution"
        case standardResolutionUrl = "standardResolution"
        case createdTime = "createdTime"
    }
    
    let postId: String
    let likeCount: Int
    let commentCount: Int
    let tags: String
    let thumbnailUrl: String?
    let lowResolutionUrl: String?
    let standardResolutionUrl: String?
    let createdTime: Date
    
    init(post: InstagramPost) {
        let id = post.id
        let likeCount = (post.likes["count"]) ?? 0
        let commentCount = (post.comments["count"]) ?? 0
        let tags = post.tags.joined(separator: ", ")
        let thumbnail = post.images[ImageType.thumbnail]?.url
        let lowResolution = post.images[ImageType.lowResolution]?.url
        let standardResolution = post.images[ImageType.standard]?.url
        
        self.postId = id
        self.likeCount = likeCount
        self.commentCount = commentCount
        self.tags = tags
        self.thumbnailUrl = thumbnail
        self.lowResolutionUrl = lowResolution
        self.standardResolutionUrl = standardResolution
        self.createdTime = post.createdTime
        
        super.init()
    }
    
    init(postId: String, like: Int, comment: Int, tags: String, thumbnail: String?, lowResolution: String?, standardResolution: String?, createdTime: Date) {
        self.postId = postId
        self.likeCount = like
        self.commentCount = comment
        self.tags = tags
        self.thumbnailUrl = thumbnail
        self.lowResolutionUrl = lowResolution
        self.standardResolutionUrl = standardResolution
        self.createdTime = createdTime
        
        super.init()
    }
    
    override class var databaseTableName: String {
        return "posts"
    }
    
    required init(row: GRDB.Row) {
        self.postId = row[ColumnName.postId.rawValue]
        self.likeCount = row[ColumnName.likeCount.rawValue]
        self.commentCount = row[ColumnName.commentCount.rawValue]
        self.thumbnailUrl = row[ColumnName.thumbnailUrl.rawValue]
        self.lowResolutionUrl = row[ColumnName.lowResolutionUrl.rawValue]
        self.standardResolutionUrl = row[ColumnName.standardResolutionUrl.rawValue]
        self.tags = row[ColumnName.tags.rawValue]
        self.createdTime = row[ColumnName.createdTime.rawValue]
        super.init(row: row)
    }
    
    override func encode(to container: inout PersistenceContainer) {
        container[ColumnName.postId.rawValue] = postId
        container[ColumnName.likeCount.rawValue] = likeCount
        container[ColumnName.commentCount.rawValue] = commentCount
        container[ColumnName.thumbnailUrl.rawValue] = thumbnailUrl
        container[ColumnName.lowResolutionUrl.rawValue] = lowResolutionUrl
        container[ColumnName.standardResolutionUrl.rawValue] = standardResolutionUrl
        container[ColumnName.tags.rawValue] = tags
        container[ColumnName.createdTime.rawValue] = createdTime
    }
    
}
