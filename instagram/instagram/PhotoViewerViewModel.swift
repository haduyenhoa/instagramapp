//
//  PhotoViewerViewModel.swift
//  instagram
//
//  Created by HDH on 29/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

/**
 View model who controls PhotoViewerViewController
 */
class PhotoViewerViewModel {
    let posts: [InstagramPostRecord]
    let startIndex: Int
    init(posts: [InstagramPostRecord], startIndex: Int) {
        self.posts = posts
        self.startIndex = startIndex
        //TODO - Improvement: should do some check on startIndex and return nil if startIndex is not compatible with given posts. 
    }
}
