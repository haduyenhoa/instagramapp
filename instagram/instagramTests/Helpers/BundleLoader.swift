//
//  BundleLoader.swift
//  instagramTests
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Foundation

import Quick

public enum BundleName: String {
    case instagramApi = "instagram_api"
}

extension QuickSpec {
    func getBundle(bundleName: BundleName) -> Bundle? {
        let bundle = Bundle(for: type(of: self)).url(forResource: bundleName.rawValue, withExtension: "bundle")!
        return Bundle(url: bundle)
    }
}
