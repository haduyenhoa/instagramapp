//
//  PhotoViewerViewModelTests.swift
//  instagramTests
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble

@testable import instagram
class PhotoViewerViewModelTests: QuickSpec {
    
    override func spec() {
        describe("a PhotoViewerViewModel") {
            var viewModel: PhotoViewerViewModel!

            context("being initialised") {
                beforeEach {
                    viewModel = PhotoViewerViewModel(posts: [], startIndex: 2)
                }
                
                it("should initialise correctly") {
                    expect(viewModel).notTo(beNil())
                    expect(viewModel.posts).to(haveCount(0))
                    expect(viewModel.startIndex) == 2
                }
            }
        }
    }
    
}
