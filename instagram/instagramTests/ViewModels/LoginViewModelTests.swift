//
//  LoginViewModelTests.swift
//  instagramTests
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble
import RxSwift
import WebKit
import OHHTTPStubs

@testable import instagram
class LoginViewModelTests: QuickSpec {
    
    override func spec() {
        describe("a LoginViewModel") {
            var viewModel: LoginViewModel!
            
            context("being initialised") {
                beforeEach {
                    viewModel = LoginViewModel()
                }
                
                it("initialises correctly") {
                    var code: String? = nil
                    var accessToken: String? = nil
                    
                    _ = viewModel.code.subscribe(onNext: { (newValue) in
                        code = newValue
                    }, onError: nil, onCompleted: nil, onDisposed: nil)
                    expect(code).toEventually(beNil())
                    
                    _ = viewModel.accessToken.subscribe(onNext: { (newValue) in
                        accessToken = newValue
                    }, onError: nil, onCompleted: nil, onDisposed: nil)
                    expect(accessToken).toEventually(beNil())
                }
            }
            
            context("when being invoked by a web view") {
                let webView = WKWebView()
                
                var code: String? = nil
                var accessToken: String? = nil
                
                beforeEach {
                    viewModel = LoginViewModel()
                    
                    //stub data for getAccessToken service
                    let jsonTokenUrl: URL! = self.getBundle(bundleName: .instagramApi)?.url(forResource: "getAccessToken", withExtension: "json")
                    expect(jsonTokenUrl).notTo(beNil())
                    
                    let testToken: Data! = try? Data.init(contentsOf: jsonTokenUrl)
                    expect(testToken).notTo(beNil())
                    
                    OHHTTPStubs.stubRequests(passingTest: { (urlRequest) -> Bool in
                        return urlRequest.url?.absoluteString == "https://api.instagram.com/oauth/access_token"
                    }, withStubResponse: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: testToken, statusCode: 200, headers: nil)
                    })
                }
                
                it("should be able to update code and access token correctly") {
                    _ = viewModel.code.subscribe(onNext: { (newValue) in
                        code = newValue
                    }, onError: nil, onCompleted: nil, onDisposed: nil)
                    
                    
                    _ = viewModel.accessToken.subscribe(onNext: { (newValue) in
                        accessToken = newValue
                    }, onError: nil, onCompleted: nil, onDisposed: nil)
                    
                    let url: URL! = URL(string: "\(InstagramConfig.redirect_uri)/?code=fakeCode")
                    expect(url).notTo(beNil())
                    
                    let request: URLRequest! = URLRequest(url: url)
                    expect(request).notTo(beNil())
                    
                    webView.navigationDelegate = viewModel
                    webView.load(request)
                    
                    viewModel.webView(webView, didReceiveServerRedirectForProvisionalNavigation: nil)
                    
                    expect(code).toEventually(equal("fakeCode"))
                    expect(accessToken).toEventually(equal("7381881402.9d5b404.65e63746102940f88691712b5b6bb672"))
                }
            }
        }
    }
    
}


