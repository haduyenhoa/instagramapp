//
//  MainViewModelTests.swift
//  instagramTests
//
//  Created by HDH on 31/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble
import GRDB
import OHHTTPStubs

@testable import instagram
class MainViewModelTests: QuickSpec {
    
    override func spec() {
        describe("a MainViewModel") {
            var dbQueue: DatabaseQueue!
            var viewModel: MainViewModel!
            
            beforeEach {
                if let databaseURL = try? FileManager.default
                    .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                    .appendingPathComponent("instagram_test.sqlite") {
                    
                    try? FileManager.default.removeItem(at: databaseURL)
                    dbQueue = try? DatabaseQueue(path: databaseURL.path)
                }
                expect(dbQueue).notTo(beNil())
            }
            
            context("being initialised with a given DatabaseQueue") {
                beforeEach {
                    viewModel = MainViewModel(dbQueue: dbQueue)
                }
                
                it("should be initialised correctly") {
                    var insertedPosts: [InstagramPostRecord]? = nil
                    _ = viewModel.posts.subscribe(onNext: { (posts) in
                        insertedPosts = posts
                    }, onError: nil, onCompleted: nil, onDisposed: nil)
                    
                    expect(insertedPosts).toNotEventually(beNil())
                    expect(insertedPosts).toEventually(beEmpty())
                }
            }
            
            context("refreshing post from back-end") {
                beforeEach {
                    viewModel = MainViewModel(dbQueue: dbQueue)
                    
                    //stub data for getMyRecentMedia service
                    let jsonRecentMediaUrl: URL! = self.getBundle(bundleName: .instagramApi)?.url(forResource: "getMyRecentMedia", withExtension: "json")
                    expect(jsonRecentMediaUrl).notTo(beNil())
                    
                    let testRecentMedias: Data! = try? Data.init(contentsOf: jsonRecentMediaUrl)
                    expect(testRecentMedias).notTo(beNil())
                    
                    OHHTTPStubs.stubRequests(passingTest: { (urlRequest) -> Bool in
                        return urlRequest.url?.absoluteString.starts(with: "https://api.instagram.com/v1/users/self/media/recent/?access_token=") ?? false
                    }, withStubResponse: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: testRecentMedias, statusCode: 200, headers: nil)
                    })
                }
                
                it("should be able to retrieve new posts correctly") {
                    var retrievedPosts: [InstagramPostRecord]? = nil
                    
                    _ = viewModel.refreshPost().subscribe(onNext: { (posts) in
                        retrievedPosts = posts
                    }, onError: nil, onCompleted: nil, onDisposed: nil)

                    expect(retrievedPosts).toEventually(haveCount(2))
                }
            }
        }
    }
    
}
