//
//  InstagramPostRecordTests.swift
//  instagramTests
//
//  Created by HDH on 01/04/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble
import GRDB

@testable import instagram
class InstagramPostRecordTests: QuickSpec {
    
    override func spec() {
        describe("a InstagramPostRecord") {
            var post: InstagramPostRecord!
            let now = Date()
            
            context("being initialised") {
                it("can be initialised from raw datas") {
                    post = InstagramPostRecord(postId: "post 1",
                                               like: 100,
                                               comment: 100,
                                               tags: "tags1, tags2",
                                               thumbnail: "http://thumbnail",
                                               lowResolution: "http://lowResolution",
                                               standardResolution: "http://standard",
                                               createdTime: now.addingTimeInterval(100))
                    
                    expect(post.postId) == "post 1"
                    expect(post.likeCount) == 100
                    expect(post.commentCount) == 100
                    expect(post.tags) == "tags1, tags2"
                    expect(post.thumbnailUrl) == "http://thumbnail"
                    expect(post.lowResolutionUrl) == "http://lowResolution"
                    expect(post.standardResolutionUrl) == "http://standard"
                    expect(post.createdTime.timeIntervalSince(now)) == 100
                }
                
                it("can be initialised from a InstagramPost") {
                    let instaPost = InstagramPost(id: "post 2",
                                                  createdTime: now.addingTimeInterval(200),
                                                  tags: ["tags1", "tags2"],
                                                  likes: ["count": 100],
                                                  comments: ["count": 100],
                                                  images: [ImageType.thumbnail: InstagramImage(width: 100, height: 100, url: "http://thumbnail"),
                                                           ImageType.lowResolution: InstagramImage(width: 100, height: 100, url: "http://lowResolution"),
                                                           ImageType.standard: InstagramImage(width: 100, height: 100, url: "http://standard")])
                    
                    post = InstagramPostRecord(post: instaPost)
                    
                    expect(post.postId) == "post 2"
                    expect(post.likeCount) == 100
                    expect(post.commentCount) == 100
                    expect(post.tags) == "tags1, tags2"
                    expect(post.thumbnailUrl) == "http://thumbnail"
                    expect(post.lowResolutionUrl) == "http://lowResolution"
                    expect(post.standardResolutionUrl) == "http://standard"
                    expect(post.createdTime.timeIntervalSince(now)) == 200
                }
                
                it("can be initialised from a GRDB row") {
                    let dateTimeFormat = DateFormatter()
                    dateTimeFormat.dateFormat = "YYYY-MM-DD HH:mm:ss" // use this to compare SQLite date because it's not precise like Date (in Swift)
                    
                    let row = GRDB.Row(["postId": "post 3",
                                        "tags": "tags1, tags2",
                                        "thumbnail": "http://thumbnail",
                                        "lowResolution": "http://lowResolution",
                                        "standardResolution": "http://standard",
                                        "createdTime": now,
                                        "likeCount": 300,
                                        "commentCount": 300])
                    
                    post = InstagramPostRecord(row: row)
                    
                    expect(post.postId) == "post 3"
                    expect(post.likeCount) == 300
                    expect(post.commentCount) == 300
                    expect(post.tags) == "tags1, tags2"
                    expect(post.thumbnailUrl) == "http://thumbnail"
                    expect(post.lowResolutionUrl) == "http://lowResolution"
                    expect(post.standardResolutionUrl) == "http://standard"
                    expect(dateTimeFormat.string(from: post.createdTime)) == dateTimeFormat.string(from: now)
                }
            }
        }
    }
    
}
