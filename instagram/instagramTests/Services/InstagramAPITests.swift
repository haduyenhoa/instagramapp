//
//  InstagramProviderTests.swift
//  instagramTests
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble
import Foundation
import Alamofire
import Moya
import OHHTTPStubs

@testable import instagram
class InstagramAPITests: QuickSpec {
    
    override func spec() {
        describe("an InstagramAPI") {
            context("being initialised") {
                it("set the correct endpoint for .getAccessToken service") {
                    let endpoint = MoyaProvider.defaultEndpointMapping(for: InstagramAPI.getAccessToken(code: "a code"))
                    expect(endpoint.url) == "https://api.instagram.com/oauth/access_token"
                    expect(endpoint.httpHeaderFields).to(beNil())
                    expect(endpoint.method).to(equal(HTTPMethod.post))
                }
                
                it("set the correct endpoint for .getMyRecentMedia service") {
                    let endpoint = MoyaProvider.defaultEndpointMapping(for: InstagramAPI.getMyRecentMedia(accessToken: "token", minId: "anId"))
                    expect(endpoint.url) == "https://api.instagram.com/v1/users/self/media/recent/?access_token=token&min_id=anId"
                    expect(endpoint.httpHeaderFields).to(beNil())
                    expect(endpoint.method).to(equal(HTTPMethod.get))
                }
            }
            
            context("there is no network issue") {
                beforeEach {
                    //stub data for getAccessToken service
                    let jsonTokenUrl: URL! = self.getBundle(bundleName: .instagramApi)?.url(forResource: "getAccessToken", withExtension: "json")
                    expect(jsonTokenUrl).notTo(beNil())
                    
                    let testToken: Data! = try? Data.init(contentsOf: jsonTokenUrl)
                    expect(testToken).notTo(beNil())
                    
                    OHHTTPStubs.stubRequests(passingTest: { (urlRequest) -> Bool in
                        return urlRequest.url?.absoluteString == "https://api.instagram.com/oauth/access_token"
                    }, withStubResponse: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: testToken, statusCode: 200, headers: nil)
                    })
                    
                    //stub data for getMyRecentMedia service
                    let jsonRecentMediaUrl: URL! = self.getBundle(bundleName: .instagramApi)?.url(forResource: "getMyRecentMedia", withExtension: "json")
                    expect(jsonRecentMediaUrl).notTo(beNil())
                    
                    let testRecentMedias: Data! = try? Data.init(contentsOf: jsonRecentMediaUrl)
                    expect(testRecentMedias).notTo(beNil())
                    
                    OHHTTPStubs.stubRequests(passingTest: { (urlRequest) -> Bool in
                        return urlRequest.url?.absoluteString.starts(with: "https://api.instagram.com/v1/users/self/media/recent/?access_token=") ?? false
                    }, withStubResponse: { (_) -> OHHTTPStubsResponse in
                        return OHHTTPStubsResponse(data: testRecentMedias, statusCode: 200, headers: nil)
                    })
                }
                
                it("should be able to get token") {
                    var token: String? = nil
                    let instagramProvider = MoyaProvider<InstagramAPI>()
                    instagramProvider.request(.getAccessToken(code: "code"), callbackQueue: nil, progress: nil) {(result) in
                        if case let .success(response) = result {
                            let jsonDecoder = JSONDecoder()
                            if let accessToken = try? jsonDecoder.decode(AccessToken.self, from: response.data) {
                                token = accessToken.token
                            }
                        }
                    }
                    
                    expect(token).toEventually(equal("7381881402.9d5b404.65e63746102940f88691712b5b6bb672"))
                }
                
                it("should be able to get my recent medias") {
                    var recentMedia: Feed? = nil
                    let instagramProvider = MoyaProvider<InstagramAPI>()
                    instagramProvider.request(.getMyRecentMedia(accessToken: "token", minId: "anId"), callbackQueue: nil, progress: nil) {(result) in
                        if case let .success(response) = result {
                            let jsonDecoder = JSONDecoder()
                            if let feed = try? jsonDecoder.decode(Feed.self, from: response.data) {
                                recentMedia = feed
                            }
                        }
                    }
                    
                    expect(recentMedia).toNotEventually(beNil())
                    expect(recentMedia?.posts.count).toEventually(equal(2))
                }
            }
        }
    }
}
