//
//  InstagramServicesTests.swift
//  instagramTests
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble

@testable import instagram
class InstagramServicesTests: QuickSpec {
    
    override func spec() {
        describe("an InstagramServices") {
            
            context("using services") {
                it("should be able to retrieve access code url") {
                    let urlRequest = InstagramServices.createGetCodeRequest()
                    expect(urlRequest?.url?.absoluteString).to(equal("https://api.instagram.com/oauth/authorize/?client_id=\(InstagramConfig.client_id)&redirect_uri=http://www.igenius.net&response_type=code&scope=public_content"))
                }
            }
        }
    }
    
}
