//
//  InstagramConfigTests.swift
//  instagramTests
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import XCTest

@testable import instagram
class InstagramConfigTests: XCTestCase {
    
    func testInstagramConfig() {
        XCTAssertEqual(InstagramConfig.client_id, "missing configuration", "replace this with your client_id to pass this test")
        XCTAssertEqual(InstagramConfig.client_scret, "missing configuration", "replace this with your client_secret to pass this test") //replace this with new configuration
        XCTAssertEqual(InstagramConfig.scope, "public_content")
        XCTAssertEqual(InstagramConfig.redirect_uri, "http://www.igenius.net")
        XCTAssertEqual(InstagramConfig.get_code_response_type, "code")
        XCTAssertEqual(InstagramConfig.grant_type, "authorization_code")
    }
}
