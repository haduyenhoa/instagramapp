//
//  KeychainStoreTests.swift
//  instagramTests
//
//  Created by HDH on 28/03/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble

@testable import instagram
class KeychainStoreTests: QuickSpec {
    
    override func spec() {
        describe("an KeychainStore") {
            context("being initialised") {
                
                beforeEach {
                    try? KeychainStore.removeAccessToken()
                }
                
                it("should be initialised correctly") {
                    expect(KeychainStore.getAccessToken()).to(beNil())
                }
                
                it("should be able to get and set access token to keychain") {
                    var result = false
                    
                    do {
                        result = try KeychainStore.updateAccessToken(token: "a token")
                    } catch {}
                    
                    expect(result).to(beTrue())
                    
                    let newToken = KeychainStore.getAccessToken()
                    expect(newToken) == "a token"
                    
                    try? KeychainStore.removeAccessToken()
                    expect(KeychainStore.getAccessToken()).to(beNil())
                }
                
                it("should be able to update to new keychain") {
                    var result = false
                    
                    do {
                        result = try KeychainStore.updateAccessToken(token: "a token")
                    } catch {}
                    
                    expect(result).to(beTrue())
                    
                    var newToken = KeychainStore.getAccessToken()
                    expect(newToken) == "a token"
                    
                    do {
                        result = try KeychainStore.updateAccessToken(token: "new token")
                    } catch {}
                    
                    expect(result).to(beTrue())
                    
                    newToken = KeychainStore.getAccessToken()
                    expect(newToken) == "new token"
                }
                
            }
        }
    }
    
}
