//
//  DatabaseManagerTests.swift
//  instagramTests
//
//  Created by HDH on 01/04/2018.
//  Copyright © 2018 HDH. All rights reserved.
//

import Quick
import Nimble
import GRDB

@testable import instagram
class DatabaseManagerTests: QuickSpec {
    
    override func spec() {
        describe("a DatabaseManager") {
            var dbQueue: DatabaseQueue!
            context("given a DatabaseQueue") {
                beforeEach {
                    if let databaseURL = try? FileManager.default
                        .url(for: .applicationSupportDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                        .appendingPathComponent("instagram_test.sqlite") {
                        
                        try? FileManager.default.removeItem(at: databaseURL)
                        dbQueue = try? DatabaseQueue(path: databaseURL.path)
                    }
                    expect(dbQueue).notTo(beNil())
                    
                    DatabaseManager.initDatabase(dbQueue: dbQueue)
                }
                
                it("can create table if does not exists") {
                    var tableCount: Int = 0
                    let queryTable = SQLRequest("SELECT name FROM sqlite_master WHERE type='table'")
                    try? dbQueue.inTransaction { db in
                        tableCount = try queryTable.fetchCount(db)
                        return .rollback
                    }
                    
                    expect(tableCount).toEventually(equal(1))
                }
                
                it("can write InstagramPostRecord to the database") {
                    let post1 = InstagramPostRecord(postId: "post 1",
                                                           like: 100,
                                                           comment: 100,
                                                           tags: "tags1, tags2",
                                                           thumbnail: "http://thumbnail",
                                                           lowResolution: "http://lowResolution",
                                                           standardResolution: "http://standard",
                                                           createdTime: Date().addingTimeInterval(-100))
                    
                    let post2 = InstagramPostRecord(postId: "post 2",
                                                    like: 200,
                                                    comment: 200,
                                                    tags: "tags3, tags4",
                                                    thumbnail: "http://thumbnail2",
                                                    lowResolution: "http://lowResolution2",
                                                    standardResolution: "http://standard2",
                                                    createdTime: Date().addingTimeInterval(-500))
                    DatabaseManager.write(dbQueue: dbQueue, records: [post1, post2])
                    
                    var postCount: Int = 0
                    let queryPost = SQLRequest("SELECT * FROM posts")
                    try? dbQueue.inTransaction { db in
                        postCount = try queryPost.fetchCount(db)
                        return .rollback
                    }
                    
                    expect(postCount).toEventually(equal(2))
                }
            }
        }
    }
    
}
